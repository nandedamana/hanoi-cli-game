/*  This file is part of Brahma, a console implementation of
 *  the classic Tower of Brahma (Tower of Hanoi) game.
 *  
 *  Copyright (C) 2017 Nandakumar Edamana <nandakumar@nandakumar.co.in>
 *
 *  Brahma is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3.
 *
 *  Brahma is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Brahma.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "graphics.h"

int graph_width, graph_height;
char **graph = NULL;
bool clear_screen = NAN_CLEAR_SCREEN_DEFAULT;

/* Prepare the graphics buffer depicting the current state of the towers */
void
graph_prepare ()
{
  /* TODO Currently I am considering tower-by-tower.
   * Will considering multiple towers in a scanline fashion make it
   * more efficient?
   */

  int tower, height;
  int tower_start_x, tower_middle_x;
  int x, i;

  graph_clear ();

  for (tower = 0; tower < NAN_TOWER_COUNT; tower++)
    {
      tower_start_x = tower * (NAN_TOWER_BASE_WIDTH + NAN_TOWER_GAP);
      tower_middle_x = tower_start_x + (NAN_TOWER_BASE_WIDTH / 2);
#define tower_end_x tower_start_x + NAN_TOWER_BASE_WIDTH

      graph[tower_middle_x][0] = '|';

      /* Draw the base */
      for (x = tower_start_x; x < tower_end_x; x++)
	{
	  /* NAN_DISC_COUNT + 1 gives the y value of the base */
	  graph[x][NAN_TOWER_HEIGHT + 1] = '_';
	}

      /* height = 0 means bottom of the tower */
      for (height = 0; height < NAN_TOWER_HEIGHT; height++)
	{
	  if (towers[tower][height] == NAN_DISC_EMPTY)
	    {
	      graph[tower_middle_x][NAN_TOWER_HEIGHT - height] = '|';
	    }
	  else
	    {
	      graph[tower_middle_x][NAN_TOWER_HEIGHT - height] =
		NAN_DISC_MIDDLE_CHAR;
	      for (i = 1; i <= disc_get_width (towers[tower][height]) / 2;
		   i++)
		{
		  graph[tower_middle_x - i][NAN_TOWER_HEIGHT - height] =
		    NAN_DISC_CHAR;
		  graph[tower_middle_x + i][NAN_TOWER_HEIGHT - height] =
		    NAN_DISC_CHAR;
		}
	    }
	}

      graph[tower_middle_x][NAN_TOWER_HEIGHT + 1] = '|';	/* Last row, middle of the current tower */
    }
}
