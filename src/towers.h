/*  This file is part of Brahma, a console implementation of
 *  the classic Tower of Brahma (Tower of Hanoi) game.
 *  
 *  Copyright (C) 2017 Nandakumar Edamana <nandakumar@nandakumar.co.in>
 *
 *  Brahma is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3.
 *
 *  Brahma is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Brahma.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _NAN_HANOI_TOWERS_H
#define _NAN_HANOI_TOWERS_H

#include <limits.h>

/* Can't be a smaller value like -1. It will ruin the disc comparison. */
#define NAN_DISC_EMPTY    INT_MAX

#define NAN_TOWER_COUNT   3
#define NAN_TOWER_HEIGHT  disc_count

#define NAN_DISC_COUNT_DEFAULT 3

extern int disc_count;

int *towers[NAN_TOWER_COUNT];
int towertops[NAN_TOWER_COUNT];

int disc_move (int src, int dest);
void towers_init ();

#endif
