/*  This file is part of Brahma, a console implementation of
 *  the classic Tower of Brahma (Tower of Hanoi) game.
 *  
 *  Copyright (C) 2017 Nandakumar Edamana <nandakumar@nandakumar.co.in>
 *
 *  Brahma is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3.
 *
 *  Brahma is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Brahma.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _NAN_HANOI_H
#define _NAN_HANOI_H

#include <stdio.h>
#include <stdlib.h>
#include <libintl.h>

/* See Nandakumar Versioning Scheme */
#define NAN_PROGNAME_LONG "Tower of Brahma command-line implementation"
#define NAN_VERSION       "1.0.1"
#define NAN_COPYRIGHT     "Copyright (C) 2017 Nandakumar Edamana <nandakumar@nandakumar.co.in>"
#define NAN_VERSION_INFO  NAN_PROGNAME_LONG" v"NAN_VERSION"\n"NAN_COPYRIGHT"\n"

#define _ gettext

#define handle_malloc(ptr); \
	if(ptr == NULL) { \
		fprintf(stderr, "%s", _("Error: Memory allocation failed.\n")); \
		exit(EXIT_FAILURE); \
	}

#ifdef __unix__
#include <time.h>
#include <bits/types/struct_timespec.h>
static inline void
nan_sleep (unsigned long int millisec)
{
  struct timespec t;
  t.tv_sec = millisec / 1000;
  millisec %= 1000;
  t.tv_nsec = millisec * 1000000;
  nanosleep (&t, NULL);		/* nanosleep() is used because usleep() is obsolete */
}
#elif defined(_WIN32) || defined(WIN32)
#include <windows.h>
#define nan_sleep(millisec) Sleep(millisec)
#endif

/* TODO make portable */
#define nan_clear_screen() printf("\x1b[2J\x1b[1;1H");

#endif
