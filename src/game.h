/*  This file is part of Brahma, a console implementation of
 *  the classic Tower of Brahma (Tower of Hanoi) game.
 *  
 *  Copyright (C) 2017 Nandakumar Edamana <nandakumar@nandakumar.co.in>
 *
 *  Brahma is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3.
 *
 *  Brahma is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Brahma.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _NAN_HANOI_GAME_H
#define _NAN_HANOI_GAME_H

#include <stdbool.h>
#include <stdio.h>

#define NAN_GAME_MODE_MANUAL_PLAY 0
#define NAN_GAME_MODE_AUTOPLAY    1
#define NAN_GAME_MODE_MENU_DRIVEN 2

#define NAN_ANIM_DELAY_MILLISEC_DEFAULT 1000

extern int game_mode;
extern long anim_delay;		/* Not unsigned so that we can detect erroneous -ve inputs */
extern int autosolve_moves_reqd;
extern int autosolve_moves_complete;
extern bool disc_count_set_by_user;	/* Not included in towers.h because towers.c and .h are much low-level */
extern bool autosolve_move_count_display_enabled;

static inline void
autosolve_display_move_count ()
{
  printf ("Move: %d of %d\n", autosolve_moves_complete, autosolve_moves_reqd);
}

int disc_move (int src, int dest);
void game_autosolve ();
int game_get_moves_required (int disc_count);
bool game_is_over ();
void game_play ();
void game_solve (int disc_count, int src, int dest, int aux, bool display,
		 bool animate);

#endif
