/*  This file is part of Brahma, a console implementation of
 *  the classic Tower of Brahma (Tower of Hanoi) game.
 *  
 *  Copyright (C) 2017 Nandakumar Edamana <nandakumar@nandakumar.co.in>
 *
 *  Brahma is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3.
 *
 *  Brahma is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Brahma.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <getopt.h>
#include <bits/getopt_core.h>

#include "main.h"
#include "game.h"
#include "graphics.h"
#include "towers.h"

int
main (int argc, char **argv)
{
  int i;
  int opt;
  int longindex;

  const struct option longopts[] = {
    {"clear", no_argument, (int *) &clear_screen, true},
    {"delay", required_argument, NULL, 'd'},
    {"discs", required_argument, NULL, 'D'},
    {"help", no_argument, NULL, 'h'},
    {"no-clear", no_argument, (int *) &clear_screen, false},
    {"play", no_argument, &game_mode, NAN_GAME_MODE_MANUAL_PLAY},
    {"solve", no_argument, &game_mode, NAN_GAME_MODE_AUTOPLAY},
    {"version", no_argument, NULL, 'v'},
    {0, 0, 0, 0}
  };

  const char *optdesc[] = {
    "Clear the screen each time before the towers are displayed.",
    "Delay between moves in autosolve mode.",
    "Number of discs.",
    "Show this help message and exit.",
    "Do not clear the screen before the towers are displayed.",
    "Display the version information and exit.",
    "Play the game yourself.",
    "Let the computer solve the puzzle."
  };

  /* TODO FIXME third param -- v only?
   */
  while ((opt = getopt_long (argc, argv, "v", longopts, &longindex)) != -1)
    {
      switch (opt)
	{
	case 0:
	  /* Option has set a flag. No immediate action required. */
	  break;
	case 'd':
	  errno = 0;
	  anim_delay = strtol (optarg, NULL, 10);
	  if (errno != 0 || anim_delay < 0)
	    {
	      fprintf (stderr, "%s", _("Error: Invalid delay.\n"));
	      exit (EXIT_FAILURE);
	    }
	  break;
	case 'D':
	  errno = 0;
	  disc_count = strtol (optarg, NULL, 10);
	  if (errno != 0 || disc_count <= 0)
	    {
	      fprintf (stderr, "%s", _("Error: Invalid disc count.\n"));
	      exit (EXIT_FAILURE);
	    }
	  disc_count_set_by_user = true;
	  break;
	case 'h':
	  for (i = 0; longopts[i].name != NULL; i++)
	    {
	      printf ("--%-10s  %s\n", longopts[i].name, _(optdesc[i]));
	    }
	  exit (EXIT_SUCCESS);
	  break;
	case 'v':
	  puts (NAN_VERSION_INFO);
	  exit (EXIT_SUCCESS);
	  break;
	default:
	  fprintf (stderr, "%s",
		   _("Invalid option(s). Use --help for help.\n"));
	  exit (EXIT_FAILURE);
	}
    }

  if (game_mode == NAN_GAME_MODE_MANUAL_PLAY)
    {
      graph_init ();
      game_play ();
      exit (EXIT_SUCCESS);
    }
  else if (game_mode == NAN_GAME_MODE_AUTOPLAY)
    {
      graph_init ();
      game_autosolve ();
      exit (EXIT_SUCCESS);
    }

  /*
   * game_mode = NAN_GAME_MODE_MENU_DRIVEN 
   */

  puts (_("\nTOWER OF BRAHMA\n---------------\n"));

  if (disc_count_set_by_user == false)
    {
      printf ("%s", _("Enter the number of discs: "));
      if (scanf ("%d", &disc_count) != 1)
	{
	  fprintf (stderr, "%s", _("Error: scanf() failure.\n"));
	  exit (EXIT_FAILURE);
	}

      if (disc_count < 1)
	{
	  fprintf (stderr, "%s", _("Error: Invalid disc count.\n"));
	  exit (EXIT_FAILURE);
	}
    }

  graph_init ();

  printf
    ("%s",
     _("Make a choice:\n\t1. Play yourself\n\t2. Autoplay the solution\n"
       "\nEnter your choice: "));
  if (scanf ("%d", &opt) != 1)
    {
      fprintf (stderr, "%s", _("Error: scanf() failure.\n"));
      exit (EXIT_FAILURE);
    }

  switch (opt)
    {
    case 1:
      game_play ();
      break;
    case 2:
      game_autosolve ();
      break;
    default:
      fprintf (stderr, "%s", _("Invalid choice.\n"));
      exit (EXIT_FAILURE);
    }

  return 0;
}
