/*  This file is part of Brahma, a console implementation of
 *  the classic Tower of Brahma (Tower of Hanoi) game.
 *  
 *  Copyright (C) 2017 Nandakumar Edamana <nandakumar@nandakumar.co.in>
 *
 *  Brahma is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3.
 *
 *  Brahma is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Brahma.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>

#include "game.h"
#include "graphics.h"
#include "main.h"
#include "towers.h"

int game_mode = NAN_GAME_MODE_MENU_DRIVEN;
long int anim_delay = NAN_ANIM_DELAY_MILLISEC_DEFAULT;

int autosolve_moves_reqd;
int autosolve_moves_complete;
bool disc_count_set_by_user = false;
bool autosolve_move_count_display_enabled = true;

void
game_autosolve ()
{
  if (autosolve_move_count_display_enabled)
    {
      autosolve_moves_reqd = game_get_moves_required (disc_count);
      autosolve_moves_complete = 0;
    }

  towers_init ();

  graph_prepare ();
  if (clear_screen)
    {
      nan_clear_screen ();
    }

  graph_display ();
  if (autosolve_move_count_display_enabled)
    {
      autosolve_display_move_count ();
    }
  nan_sleep (anim_delay);

  game_solve (disc_count, 0, 2, 1, true, true);
}

int
game_get_moves_required (int disc_count)
{
  return pow (2, disc_count) - 1;
}

bool
game_is_over ()
{
  int i;

  for (i = 0; i < disc_count; i++)
    {
      if (towers[NAN_TOWER_COUNT - 1][i] != disc_count - i)
	{
	  return false;
	}
    }

  return true;
}

void
game_play ()
{
  int src, dest;
  int moves = 0, moves_reqd;

  towers_init ();

  graph_prepare ();
  if (clear_screen)
    {
      nan_clear_screen ();
    }
  graph_display ();

  while (1)
    {
      printf
	("%s", _
	 ("Enter the source and destination towers (1-indexed, at least one -1 to quit): "));
      if (scanf ("%d%d", &src, &dest) != 2)
	{
	  fprintf (stderr, "%s", _("Error: scanf() failure.\n"));
	  exit (EXIT_FAILURE);
	}

      if (src == -1 || dest == -1)
	{
	  exit (EXIT_SUCCESS);
	}

      if (src < 1 || dest < 1 || src > disc_count || dest > disc_count)
	{
	  fprintf (stderr, "%s", _("Error: Invalid tower numbers.\n"));
	  continue;
	}

      if (disc_move (src - 1, dest - 1) != 0)
	{
	  fprintf (stderr, "%s", _("Error: Invalid move.\n"));	/* Larger source disc or empty source tower. */
	}
      else
	{
	  moves++;
	  graph_prepare ();
	  graph_display ();
	}

      if (game_is_over ())
	{
	  puts (_("CONGRATULATIONS! Game Finished!"));

	  moves_reqd = game_get_moves_required (disc_count);
	  if (moves == moves_reqd)
	    {
	      printf
		(_
		 ("You took %d moves, which is exactly the minimum number of moves required.\n"),
		 moves_reqd);
	    }
	  else
	    {
	      printf
		(_
		 ("But you took %d moves while it could have been done with %d moves.\n"),
		 moves, moves_reqd);

	    }

	  exit (EXIT_SUCCESS);
	}
    }
}

/* Recursive solution */
/* Can only be called when the towers are in the initail state. */
void
game_solve (int disc_count, int src, int dest, int aux, bool display,
	    bool animate)
{
  if (disc_count == 1)
    {
      towertops[dest]++;
      towers[dest][towertops[dest]] = towers[src][towertops[src]];

      towers[src][towertops[src]] = NAN_DISC_EMPTY;
      towertops[src]--;

      if (display)
	{
	  graph_prepare ();
	  graph_display ();
	  if (autosolve_move_count_display_enabled)
	    {
	      autosolve_moves_complete++;
	      autosolve_display_move_count ();
	    }
	  if (animate)
	    {
	      nan_sleep (anim_delay);
	    }
	}
    }
  else
    {
      game_solve (disc_count - 1, src, aux, dest, display, animate);
      game_solve (1, src, dest, aux, display, animate);
      game_solve (disc_count - 1, aux, dest, src, display, animate);
    }
}
