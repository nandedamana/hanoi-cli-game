/*  This file is part of Brahma, a console implementation of
 *  the classic Tower of Brahma (Tower of Hanoi) game.
 *  
 *  Copyright (C) 2017 Nandakumar Edamana <nandakumar@nandakumar.co.in>
 *
 *  Brahma is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3.
 *
 *  Brahma is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Brahma.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _NAN_HANOI_GRAPHICS_H
#define _NAN_HANOI_GRAPHICS_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "main.h"
#include "towers.h"

#define NAN_TOWER_GAP    1
/* disk_no is 1-indexed */
#define disc_get_width(disk_no) (disk_no * 2 + 1)
#define NAN_TOWER_BASE_WIDTH (disc_get_width(disc_count) + 2)

#define NAN_DISC_CHAR '-'
#define NAN_DISC_MIDDLE_CHAR '+'

#define NAN_CLEAR_SCREEN_DEFAULT true

extern char **graph;
extern int graph_width;
extern int graph_height;

extern bool clear_screen;	/* Clear screen each time before displaying the towers */

static inline void
graph_clear ()
{
  int i, j;

  for (i = 0; i < graph_width; i++)
    {
      for (j = 0; j < graph_height; j++)
	{
	  graph[i][j] = ' ';
	}
    }
}

static inline void
graph_display ()
{
  int x, y;

  if (clear_screen)
    {
      nan_clear_screen ();
    }

  putchar ('\n');

  for (y = 0; y < graph_height; y++)
    {
      for (x = 0; x < graph_width; x++)
	{
	  putchar (graph[x][y]);
	}

      putchar ('\n');
    }

  putchar ('\n');
}

static inline void
graph_init ()
{
  int x;

  graph_width = (NAN_TOWER_COUNT * (NAN_TOWER_BASE_WIDTH + NAN_TOWER_GAP));
  graph_height = (disc_count + 2);

  graph = malloc (sizeof (char *) * graph_width);
  handle_malloc (graph);

  for (x = 0; x < graph_width; x++)
    {
      graph[x] = malloc (sizeof (char *) * graph_height);
      handle_malloc (graph[x]);
    }
}

void graph_prepare ();

#endif
