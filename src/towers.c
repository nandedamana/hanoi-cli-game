/*  This file is part of Brahma, a console implementation of
 *  the classic Tower of Brahma (Tower of Hanoi) game.
 *  
 *  Copyright (C) 2017 Nandakumar Edamana <nandakumar@nandakumar.co.in>
 *
 *  Brahma is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3.
 *
 *  Brahma is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Brahma.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include "main.h"
#include "towers.h"

int disc_count = NAN_DISC_COUNT_DEFAULT;

int
disc_move (int src, int dest)
{
  /* Empty source tower */
  if (towertops[src] < 0)
    {
      return -1;
    }

  /* Non-empty destination tower with a smaller disk on top */
  if (towertops[dest] >= 0 &&
      (towers[src][towertops[src]] > towers[dest][towertops[dest]]))
    {
      return -1;
    }

  towertops[dest]++;
  towers[dest][towertops[dest]] = towers[src][towertops[src]];

  towers[src][towertops[src]] = NAN_DISC_EMPTY;
  towertops[src]--;

  return 0;
}

void
towers_init ()
{
  int tower, height;

  for (tower = 0; tower < NAN_TOWER_COUNT; tower++)
    {
      towers[tower] = malloc (sizeof (int) * disc_count);
      handle_malloc (towers[tower]);
    }

  /* height = 0 means bottom of the tower */

  /* The first tower */
  for (height = 0; height < disc_count; height++)
    {
      towers[0][height] = disc_count - height;
    }

  /* The rest */
  for (tower = 1; tower < NAN_TOWER_COUNT; tower++)
    {
      for (height = 0; height < disc_count; height++)
	{
	  towers[tower][height] = NAN_DISC_EMPTY;
	}
    }

  /* Set the towers' top trackers */
  for (tower = 1; tower < NAN_TOWER_COUNT; tower++)
    {
      towertops[tower] = -1;
    }

  /* The first one has all the discs initially. */
  towertops[0] = disc_count - 1;
}
